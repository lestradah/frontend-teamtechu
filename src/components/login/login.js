import { LitElement, html, css } from "lit-element";
import "@polymer/paper-card/paper-card.js";
import "@authentic/mwc-select/mwc-select.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-textfield/mwc-textfield.js";
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-form/iron-form.js';
import "@polymer/iron-image/iron-image.js";
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-spinner/paper-spinner.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '../card-colaborador/card-colaborador.js';
import "../card-colaborador-perfil-entornos/card-colaborador-perfil-entornos.js";
import "../card-colaborador-apps/card-colaborador-apps.js";
import "../card-colaborador-skills/card-colaborador-skills.js";
import "../card-colaborador-ether/card-colaborador-ether.js";
import "../card-colaborador-projects/card-colaborador-projects.js";
import { urlAPI } from "../apiconfig.js";

;
const logoBBVA = new URL('../../images/logo_bbva_azul.png', import.meta.url).toString();

const userAPI = urlAPI + "login/";

console.log(urlAPI);

class Login extends LitElement {

  static get properties() {
    return {
      userID: {
        type: String,
        notify: true
      },
      password: String,
      urlreq: String,
      mensaje: {
        type: String,
        value: ""
      },
      authenticated: {
        type: Boolean,
        value: false
      },
      bandeja: String
    };
  }

  toUpperCase(val) {
    return val.toUpperCase();
  }

  constructor() {
    super();
    this.urlreq = userAPI;
    this.authenticated = false;
  }

  static get styles() {
    return css`
        mwc-textfield {
          padding: 5px;
        }

        .centered {
          max-width: 25%;
          margin-left: auto;
          margin-right: auto;
          box-shadow: var(--shadow-elevation-2dp_-_box-shadow);
          padding: 40px;
          margin-top: 200px;
          background-color: #ffffff;
        }

        paper-button.indigo {
          background-color: #1973b8;
          color: white;
          width: -webkit-fill-available;
        }
        
        paper-button.forgoutpass {
          color: #a5a1a1;
          width: -webkit-fill-available;
        }
        
        .wrapper-btns {
          margin-top: 20px;
        }

        iron-image{
          width: 150px;
          height: 50px;
          margin-bottom: 20px;
        }

        @media (max-width: 1024px), screen and (max-height: 768px) {
          .centered {
            max-width: 60%;
            margin-bottom: 55px;
          }

          iron-image{
            width: 100px;
            height: 20px;
            padding-bottom: 15px;
          }
        }

        @media (max-width: 800px), screen and (max-height: 600px) {
          .centered {
            max-width: 75%;
            margin-bottom: 55px;
          }

          iron-image{
            width: 100px;
            height: 20px;
            padding-bottom: 15px;
          }
        }
        
        @media (max-width: 600px), screen and (max-height: 300px) {
          .centered {
            max-width: 75%;
            margin-top: 55px;
            margin-bottom: 5px;
          }
        }
        
        @media (max-width: 300px), screen and (max-height: 600px) {
          .centered {
            max-width: 75%;
            margin-top: 30px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 0px;
          }
        }

        h1 {
          text-align: center;
          font-family: "BentonSansBBVA Book";
          font-style: normal;
          font-stretch: normal;
          font-weight: 400;
          margin: 0;
          padding: 0;
        }

        .buttons {
          position: relative;
          padding: 8px 8px 8px 24px;
          margin: 0;
          color: var(--paper-dialog-button-color, var(--primary-color));
          display: var(--layout-horizontal_-_display);
          -ms-flex-direction: var(--layout-horizontal_-_-ms-flex-direction);
          -webkit-flex-direction: var(--layout-horizontal_-_-webkit-flex-direction);
          flex-direction: var(--layout-horizontal_-_flex-direction);
          -ms-flex-pack: var(--layout-end-justified_-_-ms-flex-pack);
          -webkit-justify-content: var(--layout-end-justified_-_-webkit-justify-content);
          justify-content: var(--layout-end-justified_-_justify-content);
        }

        .upperCase {
          text-transform: uppercase;
        }
        `;
  }

  render() {
    return html` ${!this.authenticated ? html`
        <form id="formLogin">
          <iron-form class="centered">
            <iron-image sizing="contain" alt="BBVA logo." fade src="${logoBBVA}"></iron-image>
            <div class="custom-parent">
              <mwc-textfield floatLabel  class="upperCase" required label="Usuario" trailingIconContent="person" id="userID" name="userID" @keyup="${this.sendInput}"></mwc-textfield>
              <mwc-textfield type="password" floatLabel  class="upperCase" required label="Clave" trailingIconContent="https" id="password" name="password" @keyup="${this.sendInput}"></mwc-textfield>
            </div>
            <div class="wrapper-btns">
              <paper-button class="indigo" @click="${this.login}">Ingresar</paper-button>
              <paper-button class="forgoutpass">¿Olvidó su contraseña?</paper-button>
            </div>
          </iron-form>
        </form>

      <iron-ajax
        id="loginAjax"
        url= "${this.urlreq}" 
        @handle-as="json"
        loading="${this.loading}"
        method="POST"
        content-type="application/json"
        @response="${this.loginSuccess}"
        @error="${this.loginFailed}"
        debounce-duration="500">
      </iron-ajax>
      
      
      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>
      `:
      html`
          ${this.bandeja}
        `}
      `;
  }

  login() {
    if (this.shadowRoot.getElementById("userID").valid &&
      this.shadowRoot.getElementById("password").valid) {

      this.loading = true;
      this.userID = this.toUpperCase(this.shadowRoot.getElementById("userID").value);
      var jrequest = {
        "userID": this.userID,
        "password": this.shadowRoot.getElementById("password").value
      };
      this.shadowRoot.getElementById("loginAjax").body = jrequest;
      this.shadowRoot.getElementById("loginAjax").generateRequest();
    } else {
      this.mensaje = "Completar usuario y clave";
      this.shadowRoot.getElementById("errorModal").opened = true;
    }
  }

  loginSuccess(event) {
    this.authenticated = true;
    var usuario = event.detail.response;
    var perfilUsuario = usuario.perfilusuario;
    console.log(perfilUsuario);
    if (perfilUsuario[0].idperfil == 1) {
      this.bandeja = html` 
        <card-colaborador userid="${this.userID}"></card-colaborador>
        <card-colaborador-perfil-entornos userid="${this.userID}"></card-colaborador-perfil-entornos>
        <card-colaborador-apps userid="${this.userID}"></card-colaborador-apps>
        <card-colaborador-skills userid="${this.userID}"></card-colaborador-skills>
        <card-colaborador-ether userid="${this.userID}"></card-colaborador-ether>
        <card-colaborador-projects></card-colaborador-projects>
      `;
    } else {
      console.log('perfil distinto a 1');
      this.bandeja = html` 
        <card-admin userid="${this.userID}" adminrol="${perfilUsuario[0].desc}"></card-admin>
      `;
    }
  }

  loginFailed(event, error) {
    if (event.detail.request.xhr.response) {
      this.mensaje = event.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }

  sendInput(evt) {
    if (evt.code === 'Enter') {
      this.login();
    }
  }
}

customElements.define("login-ap", Login);