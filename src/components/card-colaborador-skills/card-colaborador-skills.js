import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import "@fluidnext-polymer/paper-chip/paper-chips.js";
import "@fluidnext-polymer/paper-autocomplete";
import { urlAPI } from "../apiconfig.js";


import { loadEvents } from "./addSkill.js";
const collaboratorAPI = urlAPI + "collaborator/";

class CardColaboradorSkills extends LitElement {
  static get properties() {
    return {
      nombreColaborador: String,
      emailColaborador: String,
      userid: String
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  firstUpdated() {
    this.loadCollaborator();
  }

  static get styles() {
    return css`
      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }
    `;
  }

  render() {
    return html`
      <style>
        paper-card {
          --paper-card-header-text: {
            color: #ffffff;
            background-color: #1973b8;
            font-weight: 600;
          }
        }

        :host {
            --paper-chip-color : #1973B8;
        }
        
      </style>

      <div class="centered">
        <paper-card heading="Skills" alt="Skills">
          <div class="card-content">

          <!-- Skills Avanzados -->
          <h4>Alto<h4>
          <style>
              paper-autocomplete#dataInputObjects,
              paper-autocomplete#dataInputObjects2,
              paper-autocomplete#dataInputObjects3 {
                --paper-input-font-color: #1d73b2;
                --paper-autocomplete-main-color: #072146;
              }

              .chipsContainer {
                height: auto;
                overflow: auto;
              }

              

            </style>
            <dom-bind id="objectInputData">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips"
                    items="{{items}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects"
                  label="Elija sus skills avanzados"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                  <template
                    id="customTemplate"
                    slot="autocomplete-custom-template"
                  >
                    <link rel="stylesheet" href="../../../style-sugerencias.css" />
                    <paper-item
                      class="custom-item"
                      on-tap="_onSelect"
                      id$="[[_getSuggestionId(index)]]"
                      role="option"
                      aria-selected="false"
                    >
                      <div class="container info" index="[[index]]">
                        <div class="suggestion-name">[[item.nombre]]</div>
                      </div>
                      <paper-ripple></paper-ripple>
                    </paper-item>
                  </template>
                </paper-autocomplete>
              </template>
            </dom-bind>

         <!-- Skills Intermedios -->
         <h4>Medio<h4>
          <style>
            </style>
            <dom-bind id="objectInputData2">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips2"
                    items="{{items2}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects2"
                  label="Elija sus skills intermedios"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                  <template
                    id="customTemplate"
                    slot="autocomplete-custom-template"
                  >
                    <link rel="stylesheet" href="../../../style-sugerencias.css" />
                    <paper-item
                      class="custom-item"
                      on-tap="_onSelect"
                      id$="[[_getSuggestionId(index)]]"
                      role="option"
                      aria-selected="false"
                    >
                      <div class="container info" index="[[index]]">
                        <div class="suggestion-name">[[item.nombre]]</div>
                      </div>
                      <paper-ripple></paper-ripple>
                    </paper-item>
                  </template>
                </paper-autocomplete>
              </template>
            </dom-bind>
          
         <!-- Skills Basicos -->
         <h4>Bajo<h4>
          <style>
            </style>
            <dom-bind id="objectInputData3">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips3"
                    items="{{items3}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects3"
                  label="Elija sus skills basicos"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                  <template
                    id="customTemplate"
                    slot="autocomplete-custom-template"
                  >
                    <link rel="stylesheet" href="../../../style-sugerencias.css" />
                    <paper-item
                      class="custom-item"
                      on-tap="_onSelect"
                      id$="[[_getSuggestionId(index)]]"
                      role="option"
                      aria-selected="false"
                    >
                      <div class="container info" index="[[index]]">
                        <div class="suggestion-name">[[item.nombre]]</div>
                      </div>
                      <paper-ripple></paper-ripple>
                    </paper-item>
                  </template>
                </paper-autocomplete>
              </template>
            </dom-bind>
          </div>
        </paper-card>
      </div>
  `;
  }

  async getColaborador() {
    let resp = await fetch(collaboratorAPI + this.userid);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
        console.log('en getcolaborador');
        console.log(this.colaborador);
      } else {
        this.loadFailed();
      }
    }
  }

  async loadCollaborator(e) {
    let resp = await fetch(collaboratorAPI + this.userid);
    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
        var skills = [
          { id: 1, nombre: "Java" },
          { id: 2, nombre: "Spring Core" },
          { id: 3, nombre: "Spring MVC" },
          { id: 4, nombre: "Spring Security" },
          { id: 5, nombre: "Spring Boot" },
          { id: 6, nombre: "Hibernate" },
          { id: 7, nombre: "MyBatis" },
          { id: 8, nombre: "Oracle" },
          { id: 9, nombre: "PostgreSQL" },
          { id: 10, nombre: "Angular" },
          { id: 11, nombre: "JQuery" },
          { id: 12, nombre: "Polymer" },
          { id: 13, nombre: "Apache Tiles" },
          { id: 14, nombre: "Thymeleaf" },
          { id: 15, nombre: "Maven" },
          { id: 16, nombre: "Gradle" },
          { id: 17, nombre: "Message Broker" },
          { id: 18, nombre: "Cobol" }
        ];

        var objectInputData = this.shadowRoot.getElementById("objectInputData");
        var objectChips = this.shadowRoot.getElementById("objectChips");
        var objectInputData2 = this.shadowRoot.getElementById("objectInputData2");
        var objectChips2 = this.shadowRoot.getElementById("objectChips2");
        var objectInputData3 = this.shadowRoot.getElementById("objectInputData3");
        var objectChips3 = this.shadowRoot.getElementById("objectChips3");
        let __this = this;

        objectInputData.items = this.colaborador.skillsAlto;
        objectInputData.source = skills;

        objectInputData.addSelected = (evt) => {
          objectChips.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects').clear();
              __this.shadowRoot.getElementById('dataInputObjects').blur();
              let chip = objectChips.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };
        objectInputData2.items2 = this.colaborador.skillsMedio;
        objectInputData2.source = skills;

        objectInputData2.addSelected = (evt) => {
          objectChips2.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects2').clear();
              __this.shadowRoot.getElementById('dataInputObjects2').blur();
              let chip = objectChips2.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips2.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

        objectInputData3.items3 = this.colaborador.skillsBajo;
        objectInputData3.source = skills;

        objectInputData3.addSelected = (evt) => {
          objectChips3.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects3').clear();
              __this.shadowRoot.getElementById('dataInputObjects3').blur();
              let chip = objectChips3.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips3.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

        this.deleteChips(this);
      } else {
        this.loadFailed();
      }
    }
  }

  loadFailed(e) {
    if (e.detail.request.xhr.response) {
      this.mensaje = e.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }

  async updatePerfil() {
    await this.getColaborador();
    this.colaborador.skillsAlto = this.shadowRoot.getElementById("objectInputData").items;
    this.colaborador.skillsMedio = this.shadowRoot.getElementById("objectInputData2").items2;
    this.colaborador.skillsBajo = this.shadowRoot.getElementById("objectInputData3").items3;
    this.colaborador.modificado = true;

    let resp = await fetch(collaboratorAPI + this.userid,
      { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(this.colaborador) });

    if (resp.ok) {
      if (resp.status == 200) {
        let data = await resp.json();
        console.log(data);
      } else {
        this.loadFailed();
      }
    }
  }

  deleteChips(e) {
    let card = e;
    let $this = this;
    setTimeout(function (ex) {
      let allPaperChips = card.shadowRoot.getElementById('objectChips').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
      let allPaperChips2 = card.shadowRoot.getElementById('objectChips2').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips2.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips2').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
      let allPaperChips3 = card.shadowRoot.getElementById('objectChips3').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips3.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips3').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
    }, 300);
  }
  respuestaRecibida(e) {
  }
}

customElements.define("card-colaborador-skills", CardColaboradorSkills);
