
export function loadEvents(idColaborador, element){

    document.addEventListener("DOMContentLoaded", event => {
        
        console.log(idColaborador);
        
        let domBind = element.shadowRoot.querySelector("dom-bind#objectInputData");
        let domBind2 = element.shadowRoot.querySelector("dom-bind#objectInputData2");
        let domBind3 = element.shadowRoot.querySelector("dom-bind#objectInputData3");


        //GET ALL APPS
        var apps = [
          {
            id: "1",
            nombre: "JAVA",
          },
          {
            id: "2",
            nombre: "Spring",
          },
          {
            id: "3",
            nombre: "JBOSS",
          }
        ];

        domBind.source = apps;
        domBind2.source = apps;
        domBind3.source = apps;



        //GET APPS COLABORADOR
        domBind.items = [];
        domBind.items2 = [];
        domBind.items3 = [];

        domBind.addSelected = evt => {
          domBind.$.objectChips.add(evt.detail.value);
          //put app 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").clear();
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").blur();
          }, 100);
        };

        domBind2.addSelected = evt => {
          domBind2.$.objectChips2.add(evt.detail.value);
          //put app 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects2").clear();
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects2").blur();
          }, 100);
        };


        domBind3.addSelected = evt => {
          domBind3.$.objectChips3.add(evt.detail.value);
          //put app 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects3").clear();
          }, 100);
        };
      });

}