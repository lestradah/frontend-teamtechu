import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";

import "@vaadin/vaadin-grid/vaadin-grid.js";
import "@vaadin/vaadin-grid/vaadin-grid-selection-column.js";
import "@vaadin/vaadin-grid/vaadin-grid-sort-column.js";
import "@vaadin/vaadin-grid/vaadin-grid-filter-column.js";

import { loadProjects } from "./data-proyectos.js";

class CardColaboradorProjects extends LitElement {
  static get properties() {
    return {
      proyectos: { type: Array }
    };
  }

  constructor() {
    super();
    this.proyectos = [
      { ID: 1, PROYECTO: "T-Refiero", FTE: "0.5", PI: "PI-I 2019" },
      { ID: 2, PROYECTO: "T-Refiero 1.0", FTE: "0.4", PI: "PI-III 2018" },
      { ID: 3, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 4, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 5, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 6, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 7, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 8, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 9, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 10, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 11, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 12, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 13, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 14, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 15, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" },
      { ID: 16, PROYECTO: "New Beginning", FTE: "0.5", PI: "PI-II 2019" }
    ];
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback(){
    super.disconnectedCallback();
  }

  firstUpdated() {
    loadProjects(this, this.proyectos);
  }

  static get styles() {
    return css`
      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      .centered2 {
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }
    `;
  }

  render() {
    return html`
      <style>
        paper-card {
          --paper-card-header-text: {
            color: #ffffff;
            background-color: #1973b8;
            font-weight: 600;
          }
        }
      </style>

      <div class="centered">
        <paper-card heading="Historico de proyectos" alt="Proyectos">
          <div class="card-content">
            <div class="centered2">
              <vaadin-grid
                theme="row-dividers"
                column-reordering-allowed
                multi-sort
              >
                <vaadin-grid-sort-column
                  width="1em"
                  path="ID"
                  headeer="ID"
                ></vaadin-grid-sort-column>
                <vaadin-grid-filter-column
                  width="1em"
                  path="PROYECTO"
                  header="PROYECTO"
                ></vaadin-grid-filter-column>
                <vaadin-grid-sort-column
                  width="1em"
                  path="FTE"
                  header="FTE"
                ></vaadin-grid-sort-column>

                <vaadin-grid-filter-column
                  width="1em"
                  path="PI"
                  header="PI"
                ></vaadin-grid-filter-column>
              </vaadin-grid>
            </div>
          </div>
        </paper-card>
      </div>
    `;
  }
}

customElements.define("card-colaborador-projects", CardColaboradorProjects);
