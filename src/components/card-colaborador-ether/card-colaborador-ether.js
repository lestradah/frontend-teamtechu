import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import "@fluidnext-polymer/paper-chip/paper-chips.js";
import "@fluidnext-polymer/paper-autocomplete";
import { urlAPI } from "../apiconfig.js";

const collaboratorAPI = urlAPI + "collaborator/";

class CardColaboradorEther extends LitElement {
  static get properties() {
    return {
      nombreColaborador: String,
      emailColaborador: String,
      userid: String
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  static get styles() {
    return css`
      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }
    `;
  }

  firstUpdated() {
    this.loadCollaborator();
  }

  async getColaborador() {
    let resp = await fetch(collaboratorAPI + this.userid);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
      } else {
        this.loadFailed();
      }
    }
  }

  async loadCollaborator() {
    let resp = await fetch(collaboratorAPI + this.userid);
    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
        var ether = [
          { id: 1, nombre: "APX" },
          { id: 2, nombre: "Cells" },
          { id: 3, nombre: "Datio" },
          { id: 4, nombre: "ASO" }
        ];

        var objectInputData = this.shadowRoot.getElementById("objectInputData");
        var objectChips = this.shadowRoot.getElementById("objectChips");
        let __this = this;

        objectInputData.items = this.colaborador.ether;
        objectInputData.source = ether;

        objectInputData.addSelected = (evt) => {
          objectChips.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects').clear();
              __this.shadowRoot.getElementById('dataInputObjects').blur();
              let chip = objectChips.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };
        this.deleteChips(this);
      }else {
        this.loadFailed();
      }
    }
  }

  loadFailed(e) {
    if (e.detail.request.xhr.response) {
      this.mensaje = e.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }

  deleteChips(e) {
    let card = e;
    let $this = this;
    setTimeout(function (ex) {
      let allPaperChips = card.shadowRoot.getElementById('objectChips').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
    }, 300);
  }

  async updatePerfil() {
    await this.getColaborador();
    this.colaborador.ether = this.shadowRoot.getElementById("objectInputData").items;
    this.colaborador.modificado = true;

    let resp = await fetch(collaboratorAPI + this.userid,
      { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(this.colaborador) });

    if (resp.ok) {
      if (resp.status == 200) {
        let data = await resp.json();
        console.log(data);
      } else {
        this.loadFailed();
      }
    }
  }

  respuestaRecibida(e) {
  }

  render() {
    console.log('render-ether');
    return html`
      <style>
        paper-card {
          --paper-card-header-text: {
            color: #ffffff;
            background-color: #1973b8;
            font-weight: 600;
          }
        }

        :host {
          --paper-chip-color: #1973b8;
        }
      </style>

      <div class="centered">
        <paper-card heading="Ether" alt="Ether">
          <div class="card-content">
            <style>
              paper-autocomplete#dataInputObjects {
                --paper-input-font-color: #1d73b2;
                --paper-autocomplete-main-color: #072146;
              }

              .chipsContainer {
                height: auto;
                overflow: auto;
              }
            </style>
            <dom-bind id="objectInputData">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips"
                    items="{{items}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects"
                  label="Elija sus skills ether"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                <template id="customTemplate" slot="autocomplete-custom-template">
                <link rel="stylesheet" href="../../../style-sugerencias.css">
                <paper-item class="custom-item" on-tap="_onSelect" id$="[[_getSuggestionId(index)]]" role="option" aria-selected="false">
                    <div class="container info" index="[[index]]">
                        <div>[[item.nombre]]</div>
                    </div>
                    <paper-ripple></paper-ripple>
                </paper-item>
            </template>
                </paper-autocomplete>
              </template>
            </dom-bind>
          </div>
        </paper-card>
      </div>


      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>

    `
  }
}

customElements.define("card-colaborador-ether", CardColaboradorEther);
