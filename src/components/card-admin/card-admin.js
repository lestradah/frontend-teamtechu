import { LitElement, html, css } from "lit-element";

import "@authentic/mwc-top-app-bar/mwc-top-app-bar.js";
import "@authentic/mwc-icon-button/mwc-icon-button.js";
import "@authentic/mwc-icon/mwc-icon.js";
import "@authentic/mwc-drawer/mwc-drawer.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@polymer/iron-image/iron-image.js";
import "./filtro-colaborador.js";
import "./card-report-staffing.js";
import "./card-report-colaborador.js";
import "./card-report-pendientes.js";
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';

import Navigo from "navigo";

import { openDrawer } from "./eventDrawer";
import { urlAPI } from "../apiconfig.js";

const userAPI = urlAPI + "logout/";

const apiCollaborator = urlAPI + "collaborator/";

const logoBBVA = new URL('../../images/logo_bbva.png', import.meta.url).toString();

class CardAdmin extends LitElement {
  static get properties() {
    return {
      isColaboradorSelected: { type: Boolean },
      isStaffingSelected: { type: Boolean },
      isPendienteSelected: { type: Boolean },
      router: { type: Object },
      userID: { type: String },
      adminUserName: { type: String },
      adminRol: { type: String },
      mensaje: {
        type: String,
        value: ""
      },
      urlreq: String
    };
  }

  constructor() {
    super();
    this.isColaboradorSelected = true;
    this.isStaffingSelected = false;
    this.isPendienteSelected = false;
    this.isSalir = false;
    this.userID = "";
    this.adminUserName = "";
    this.adminRol = "";
    this.urlreq = userAPI;

    let router = new Navigo("/", true, "#!");

    router.on("staffing", (params, query) => {
      this.isColaboradorSelected = false;
      this.isStaffingSelected = true;
      this.isPendienteSelected = false;
      this.isSalir = false;

      if (query == "") {
        this.router = html`<card-report-staffing></card-report-staffing>`;
      } else {
        let replaceQuery = decodeURI(query);
        let queryItems = replaceQuery.split('&');
        let isAsignado = new Boolean(queryItems[0].split('=')[1] === "true");
        let disciplina = queryItems[1].split('=')[1];

        if (isAsignado.valueOf()) {

          this.router = html`<card-report-colaborador isasignado  disciplina=${disciplina}></card-report-colaborador>`;
        } else {

          this.router = html`<card-report-colaborador disciplina=${disciplina}></card-report-colaborador>`;
        }
      }

    })
      .on("staffing/colaborador/:userID", (params, query) => {
        this.isColaboradorSelected = false;
        this.isStaffingSelected = true;
        this.isPendienteSelected = false;
        this.isSalir = false;
        this.userID = params.userID;
        this.router = html`<card-colaborador userid="${this.userID}"></card-colaborador>
        <card-colaborador-perfil-entornos userid="${this.userID}"></card-colaborador-perfil-entornos>
        <card-colaborador-apps userid="${this.userID}"></card-colaborador-apps>
        <card-colaborador-skills userid="${this.userID}"></card-colaborador-skills>
        <card-colaborador-ether userid="${this.userID}"></card-colaborador-ether>
        <card-colaborador-projects></card-colaborador-projects>`;

      })
      .on("pendientes", () => {
        this.isColaboradorSelected = false;
        this.isStaffingSelected = false;
        this.isPendienteSelected = true;
        this.isSalir = false;
        this.router = html`<card-report-pendientes></card-report-pendientes>`;

      })
      .on("pendientes/colaborador/:userID", (params, query) => {
        this.isColaboradorSelected = false;
        this.isStaffingSelected = false;
        this.isPendienteSelected = true;
        this.isSalir = false;
        this.userID = params.userID;
        this.router = html`<card-colaborador userid="${this.userID}" isvalidacion></card-colaborador>
        <card-colaborador-perfil-entornos userid="${this.userID}"></card-colaborador-perfil-entornos>
        <card-colaborador-apps userid="${this.userID}"></card-colaborador-apps>
        <card-colaborador-skills userid="${this.userID}"></card-colaborador-skills>
        <card-colaborador-ether userid="${this.userID}"></card-colaborador-ether>
        <card-colaborador-projects></card-colaborador-projects>`;

      })
      .on("filtro-colaborador", () => {
        this.isColaboradorSelected = true;
        this.isStaffingSelected = false;
        this.isPendienteSelected = false;
        this.isSalir = false;
        this.router = html`<filtro-colaborador></filtro-colaborador>`;

      })
      .on("filtro-colaborador/colaborador/:userID", (params, query) => {
        this.isColaboradorSelected = true;
        this.isStaffingSelected = false;
        this.isPendienteSelected = false;
        this.isSalir = false;
        this.userID = params.userID;
        this.router = html`<card-colaborador userid="${this.userID}"></card-colaborador>
        <card-colaborador-perfil-entornos userid="${this.userID}"></card-colaborador-perfil-entornos>
        <card-colaborador-apps userid="${this.userID}"></card-colaborador-apps>
        <card-colaborador-skills userid="${this.userID}"></card-colaborador-skills>
        <card-colaborador-ether userid="${this.userID}"></card-colaborador-ether>
        <card-colaborador-projects></card-colaborador-projects>`;

      })
      .on("salir", () => {
        this.isColaboradorSelected = false;
        this.isStaffingSelected = false;
        this.isPendienteSelected = false;
        this.isSalir = true;
      }).on("", () => {
        this.isColaboradorSelected = true;
        this.isStaffingSelected = false;
        this.isPendienteSelected = false;
        this.isSalir = false;
        this.router = html`<filtro-colaborador></filtro-colaborador>`;

      });

    router.resolve();
  }

  firstUpdated() {
    openDrawer(this);
    this.getDataAdmin();

  }

  async getDataAdmin() {

    let response = await fetch(apiCollaborator + this.userID);
    let dataAdmin = await response.json();
    this.adminUserName = dataAdmin.nombre + " " + dataAdmin.apepaterno;

  }

  static get styles() {
    return css`
      .flex-container {
        padding: 0;
        margin: 0;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      iron-image.logo-bbva {
        width: 150px;
        height: 50px;
      }

      .center-icon-text {
        display: flex;
        align-items: center;
      }


    `;
  }

  render() {
    return html`
      <style>
        .mwc-top-app-bar {
          left: 0;
        }
        :host {
          --mdc-theme-primary: #072146;
          --mdc-theme-text-primary-on-light: #072146;
        }

        .drawer-content {
          padding: 0px 16px 0 16px;
        }

        .main-content {
          min-height: 40px;
          padding: 48px 18px 0 18px;
        }

        a {
          text-decoration: none;
          color: #072146;
        }
      </style>

      <mwc-drawer hasHeader type="modal">
        <span slot="title">${this.adminUserName}</span>
        
        <span slot="subtitle">${this.userID + " - " + this.adminRol}</span>
        <div class="drawer-content">
          <p>Busqueda</p>
          <mwc-list>
            <a href="#!/filtro-colaborador"><mwc-list-item value="colaborador" ?selected=${this.isColaboradorSelected}>
            <mwc-icon-button
                    icon="home"
                    slot="graphic"
            ></mwc-icon-button>
            COLABORADOR</mwc-list-item></a>
          </mwc-list>
          <p>Reportes</p>
          <mwc-list>
            <a href="#!/staffing"><mwc-list-item value="staffing" ?selected=${this.isStaffingSelected}>STAFFING</mwc-list-item></a>
            <a href="#!/pendientes"><mwc-list-item value="pendientes" ?selected=${this.isPendienteSelected}>PENDIENTES</mwc-list-item></a>
          </mwc-list>

          <mwc-list @click="${this.logout}">
            <a href="#"><mwc-list-item value="salir" ?selected=${this.isSalir}>
            <mwc-icon-button
                    icon="exit_to_app"
                    slot="graphic"
            ></mwc-icon-button>
            Salir</mwc-list-item></a>
          </mwc-list>
        </div>

        <div slot="appContent">
          <mwc-top-app-bar>
            <mwc-icon-button
              icon="menu"
              slot="navigationIcon"
            ></mwc-icon-button>
            <div slot="title" id="title">
              <div class="flex-container">
                <iron-image
                  class="logo-bbva"
                  sizing="contain"
                  alt="BBVA logo."
                  src="${logoBBVA}"
                ></iron-image>
                <span style="margin-left: 10px"></span>
                <div style="margin-top:auto">SISTEMA DE GESTION DE SKILLS</div>
              </div>
            </div>
          </mwc-top-app-bar>
          <div class="main-content"></div>
        </div>
      </mwc-drawer>

      ${this.router}
      
      <iron-ajax
        id="logoutAjax"
        url= "${this.urlreq}" 
        @handle-as="json"
        loading="${this.loading}"
        method="POST"
        content-type="application/json"
        @response="${this.logoutSuccess}"
        @error="${this.logoutFailed}"
        debounce-duration="500">
      </iron-ajax>

      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>
      
    `;
  }

  logout() {
    var jrequest = {
      "userID": this.userID
    };
    this.shadowRoot.getElementById("logoutAjax").body = jrequest;
    this.shadowRoot.getElementById("logoutAjax").generateRequest();
  }

  logoutSuccess(event) {
    location.href = location.origin+location.pathname;
  }

  logoutFailed(event, error) {
    if (event.detail.request.xhr.response) {
      this.mensaje = event.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }
}

customElements.define("card-admin", CardAdmin);
