import { LitElement, html, css } from "lit-element";
import { flexFactor } from "@collaborne/lit-flexbox-literals";
import { until } from "lit-html/directives/until.js";

import "@polymer/paper-card/paper-card.js";
import "@authentic/mwc-select/mwc-select.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-textfield/mwc-textfield.js";
import "@authentic/mwc-button/mwc-button.js";
import "@authentic/mwc-icon-button/mwc-icon-button.js";
import "@polymer/paper-dialog/paper-dialog.js";
import "@polymer/paper-button/paper-button.js";
import { urlAPI } from "../apiconfig.js";

const imgProfile = new URL("../../images/profile_img.png", import.meta.url).toString();
const apiStaffing = urlAPI + "staff/";

class FiltroColaborador extends LitElement {
  static get properties() {
    return {
      especialidades: { type: Array },
      projects: { type: Array },
      dataColaborador: { type: Object },
      msgBusqueda : {type : String}
    };
  }

  constructor() {
    super();

    this.organizacionAgile = [
      {
        id: 1,
        nombreDiscplina: "Retail Customer Systems",
        especialidad: [
          { id: 1, nombreEspecialidad: "Mobile" },
          { id: 2, nombreEspecialidad: "Web y Portales" },
          { id: 3, nombreEspecialidad: "Agentes Correponsales" },
          { id: 4, nombreEspecialidad: "ATMs y Autoservicios" },
          { id: 5, nombreEspecialidad: "Branches & Sales Advisory" },
          { id: 6, nombreEspecialidad: "Call Center" }
        ]
      },

      {
        id: 2,
        nombreDiscplina: "Enterprise + CIB Systems",
        especialidad: [
          {
            id: 1,
            nombreEspecialidad: "Branches, Sales Advisory Mngt Systems"
          },
          { id: 2, nombreEspecialidad: "Netcash/Mobile" },
          { id: 3, nombreEspecialidad: "Productos Especializados" }
        ]
      },

      {
        id: 3,
        nombreDiscplina: "Market Systems",
        especialidad: [
          { id: 1, nombreEspecialidad: "Asset Mng / Private Banking" },
          { id: 2, nombreEspecialidad: "Global Markets" }
        ]
      },

      {
        id: 4,
        nombreDiscplina: "Corporate Systems",
        especialidad: [
          { id: 1, nombreEspecialidad: "Finance" },
          { id: 2, nombreEspecialidad: "Risk" },
          { id: 3, nombreEspecialidad: "Legal / Audit" },
          { id: 4, nombreEspecialidad: "Talento y Cultura" },
          { id: 2, nombreEspecialidad: "Engineering" }
        ]
      }
    ];

    this.especialidades = [];
    this.projects = [];
    this.disciplinaSelected = "seleccioneDisciplina";
    this.especialidadSelected = "seleccioneEspecialidad";
    this.proyectoSelected = "seleccioneProyecto";
    this.msgBusqueda = "";
  }

  static get styles() {
    return css`
      paper-card {
        width: 100%;
        border-radius: 15px;
      }

      .flex-container {
        padding: 0;
        margin-top: 10px;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
      }

      .filtro {
        padding-left: 10px;
        padding-right: 10px;
        width: auto;
        ${flexFactor}
      }

      .search {
        width: 65%;
        margin-left: auto;
        margin-right: auto;
      }

      .search-btn {
        text-align: center;
        --mdc-theme-primary: #1973b8;
      }

      .searchResult {
        text-align: center;
      }

      .colaborador-grid {
        margin-top: 30px;
        width: 90%;
        margin-left: auto;
        margin-right: auto;
      }

      iron-image.foto-perfil-list {
        width: 56px;
        height: 56px;
        border-radius: 50%;
      }

      mwc-textfield {
        --mdc-theme-primary: #072146;
      }

      mwc-select {
        --mdc-theme-primary: #072146;
      }

      paper-button{
        color: #1973b8;
      }

      a {
        text-decoration: none;
        color: #072146;
      }

    `;
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  _handleChangeDisciplina(evt) {
    this.shadowRoot
      .getElementById("select_especialidad")
      ._setTextContent("Seleccione especialidad");
    
    this.especialidadSelected = "seleccioneEspecialidad";
    this.proyectoSelected = "seleccioneProyecto";

    this.especialidades = [];

    this.disciplinaSelected = evt.detail.value;

    for (const discplina of this.organizacionAgile) {
      if (discplina.nombreDiscplina == evt.detail.value) {
        this.especialidades = discplina.especialidad;
        break;
      }
    }
  }

  _handleChangeEspecialidad(evt) {
    this.shadowRoot
      .getElementById("select_proyecto")
      ._setTextContent("Seleccione proyecto");

    this.projects = [];


    this.especialidadSelected = evt.detail.value == 'seleccioneEspecialidad' ? 'seleccioneEspecialidad' : evt.detail.value;

    this.searchProjects(this.disciplinaSelected, this.especialidadSelected);
  }

  _handleChangeProyecto(evt) {
 
    this.proyectoSelected = evt.detail.value == 'seleccioneProyecto' ? 'seleccioneProyecto' : evt.detail.value;

  }

  async searchProjects(_disciplina, _especialidad) {
    let resp = await fetch(
      apiStaffing +
        "?disciplina=" +
        _disciplina +
        "&especialidad=" +
        _especialidad +
        "&onlyproyecto=" +
        true
    );
    this.projects = await resp.json();
  }

  firstUpdated() {
    this.shadowRoot
      .getElementById("select_disciplina")
      .addEventListener("change", this._handleChangeDisciplina.bind(this));
    this.shadowRoot
      .getElementById("select_especialidad")
      .addEventListener("change", this._handleChangeEspecialidad.bind(this));
    this.shadowRoot
      .getElementById("select_proyecto")
      .addEventListener("change", this._handleChangeProyecto.bind(this));
  }

  async buscarColaborador() {

    let nroRegistro = this.shadowRoot
    .getElementById("nroRegistro")
    .value.trim().toUpperCase();

    if (this.disciplinaSelected == 'seleccioneDisciplina' && nroRegistro == "")
    { 

      let dialogAlerta = this.shadowRoot.getElementById("alertBusqueda");

      dialogAlerta.open();

      return;
    }



    let endPoint = apiStaffing;

    if (nroRegistro == "") {
      endPoint +=
        "?disciplina=" +
        this.disciplinaSelected +
        (this.especialidadSelected == 'seleccioneEspecialidad' ? '' : "&especialidad=" +
        this.especialidadSelected )+
        (this.proyectoSelected == 'seleccioneProyecto' ? '' : "&proyecto=" +
        this.proyectoSelected);
    } else {
      endPoint += nroRegistro;
    }

    let resp = await fetch(endPoint);

    if(resp.ok){
      this.dataColaborador = await resp.json();

      if(this.dataColaborador.message == "No such staffing" || this.dataColaborador.length == 0){
        this.msgBusqueda = html`<p class="searchResult">No se encontro colaborador</p>`;
      } else {
        this.msgBusqueda = html`${this.dataColaborador.map(
          (i, index) =>
            html`<a href="#!/filtro-colaborador/colaborador/${i.userID}">
              <mwc-list-item type="two-line" avatarList value=${i.userID} @click=${this.selectColaborador}>
                <iron-image
                  slot="graphic"
                  class="foto-perfil-list"
                  sizing="contain"
                  alt="profile"
                  preload
                  src="${imgProfile}"
                ></iron-image>
                <span slot="primary"
                  >${i.userID + " - " + i.nombre}</span
                >
                <span slot="secondary"
                  >${"Proyecto: " + i.proyecto}</span
                >
                <span slot="meta">FTE: ${i.fte}</span>
              </mwc-list-item></a>

              <mwc-list-divider padded></mwc-list-divider>
            `
        )}`;
      }
    } else {
      this.msgBusqueda = html`<p>Error servicio staffing</p>`;
    }



  }


  selectColaborador(evt){
    console.log(evt.currentTarget.value);

  }


  limpiarFiltros(){

    this.shadowRoot
    .getElementById("select_disciplina")
    ._setTextContent("Seleccione disciplina");
    this.disciplinaSelected = "seleccioneDisciplina";

    this.shadowRoot
      .getElementById("select_especialidad")
      ._setTextContent("Seleccione especialidad");
    
    this.especialidadSelected = "seleccioneEspecialidad";

    this.shadowRoot
      .getElementById("select_proyecto")
      ._setTextContent("Seleccione proyecto");
      this.proyectoSelected = "seleccioneProyecto";

      this.shadowRoot
      .getElementById("nroRegistro")
      .value = '';

    this.especialidades = [];
    this.projects = [];
    this.dataColaborador = [];
    this.msgBusqueda = '';

  }



  render() {
    return html`
      <div class="centered">
        <paper-card heading="BUSCAR COLABORADOR" alt="colaborador">
          <div>
            <style></style>

            <div class="flex-container">
              <div class="filtro">
                <mwc-select placeholder="Disciplina" id="select_disciplina">
                  <mwc-menu>
                    <mwc-list>
                      <mwc-list-item value="seleccioneDisciplina"
                        >Seleccione disciplina</mwc-list-item
                      >
                      ${this.organizacionAgile.map(
                        i =>
                          html`
                            <mwc-list-item value=${i.nombreDiscplina}
                              >${i.nombreDiscplina}</mwc-list-item
                            >
                          `
                      )}
                    </mwc-list>
                  </mwc-menu>
                </mwc-select>
              </div>
              <div class="filtro">
                <mwc-select placeholder="Especialidad" id="select_especialidad">
                  <mwc-menu>
                    <mwc-list>
                      <mwc-list-item value="seleccioneEspecialidad"
                        >Seleccione especialidad</mwc-list-item
                      >
                      ${this.especialidades.map(
                        i =>
                          html`
                            <mwc-list-item value=${i.nombreEspecialidad}
                              >${i.nombreEspecialidad}</mwc-list-item
                            >
                          `
                      )}
                    </mwc-list>
                  </mwc-menu>
                </mwc-select>
              </div>
              <div class="filtro">
                <mwc-select placeholder="Proyecto" id="select_proyecto">
                  <mwc-menu>
                    <mwc-list>
                      <mwc-list-item value="seleccioneProyecto"
                        >Seleecione proyecto</mwc-list-item
                      >
                      ${this.projects.map(
                        i =>
                          html`
                            <mwc-list-item value=${i}>${i}</mwc-list-item>
                          `
                      )}
                    </mwc-list>
                  </mwc-menu>
                </mwc-select>
              </div>
            </div>
            <div class="search">
              <mwc-textfield
                floatLabel
                trailingIconContent="search"
                label="Nro. Registro o nombres"
                maxLength="50"
                id="nroRegistro"
              ></mwc-textfield>
            </div>
            <div class="flex-container">
              <div class="filtro search-btn">
                <mwc-button
                  @click=${this.buscarColaborador}
                  raised
                  label="BUSCAR"
                  icon="search"
                  id="search"
                ></mwc-button>
              </div>
              <div class="filtro search-btn">
                <mwc-button
                  @click=${this.limpiarFiltros}
                  raised
                  label="LIMPIAR"
                  icon="delete"
                  id="clear"
                ></mwc-button>
              </div>
            </div>

            <div class="colaborador-grid">
              <mwc-list type="two-line" avatarList singleSelection id="lista_colaboradores">
                ${this.msgBusqueda}
              </mwc-list>

              <paper-dialog id="alertBusqueda"  with-backdrop>
                <h2>Alerta</h2>
                <p>Debe elegir una disciplina o colocar un numero de registro</p>
                <div class="buttons">
                  <paper-button dialog-dismiss autofocus>Aceptar</paper-button>
                </div>
              </paper-dialog>

            </div>
          </div>
        </paper-card>
      </div>
    `;
  }
}

customElements.define("filtro-colaborador", FiltroColaborador);
