import { LitElement, html, css } from "lit-element";
import { flexFactor } from "@collaborne/lit-flexbox-literals";

import "@polymer/paper-card/paper-card.js";
import "@authentic/mwc-select/mwc-select.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-textfield/mwc-textfield.js";
import "@authentic/mwc-button/mwc-button.js";
import "@authentic/mwc-switch/mwc-switch.js";
import { urlAPI } from "../apiconfig.js";

const imgProfile = new URL("../../images/profile_img.png", import.meta.url).toString();
const apiStaffing = urlAPI + "staff/";
const apiCollaborator = urlAPI + "collaborator/";

class CardReportColaborador extends LitElement {
  static get properties() {
    return {
      isAsignado: { type: Boolean },
      disciplina: { type: String },
      colaboradores: { type: Array },
      colaboradoresTotal: { type: Array }
    };
  }

  constructor() {
    super();
    this.isAsignado = false;
    this.disciplina = "";
    this.iconSection = "";
    this.fteSection = "";
    //this.poblarListaColaborador = () => {};
    this.colaboradores = [];
    this.colaboradoresTotal = [];
  }

  static get styles() {
    return css`
      paper-card {
        width: 100%;
        border-radius: 15px;
      }

      .flex-container {
        padding: 0;
        margin: 0;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
      }

      .filtro {
        padding-left: 10px;
        padding-right: 10px;
        width: auto;
        ${flexFactor}
      }

      .search {
        width: 65%;
        margin-left: auto;
        margin-right: auto;
      }

      .search-btn {
        text-align: center;
        --mdc-theme-primary: #1973b8;
      }

      .colaborador-grid {
        margin-top: 30px;
        width: 90%;
        margin-left: auto;
        margin-right: auto;
      }

      iron-image.foto-perfil-list {
        width: 56px;
        height: 56px;
        border-radius: 50%;
      }

      mwc-switch {
        --mdc-theme-primary: #072146;
        --mdc-theme-secondary: #1973b8;
      }

      .filtrofte {
        text-align: center;
        margin-top: 10px;
      }

      a {
        text-decoration: none;
        color: #072146;
      }

    `;
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  firstUpdated() {
    console.log(this.isAsignado);
    console.log(this.disciplina);

    this.shadowRoot
      .getElementById("swtFTE")
      .addEventListener("change", this.filtrarFTE.bind(this));

    this.poblarListaColaborador();
  }

  async poblarListaColaborador() {
    let apiSellected = "";
    if (this.isAsignado) {
      apiSellected = apiStaffing + "?disciplina=" + encodeURIComponent(this.disciplina);
    } else {
      apiSellected =
        apiCollaborator +
        "?disciplina=" +
        this.disciplina +
        "&noasignado=" +
        true;
    }

    let resp = await fetch(apiSellected);
    let listaColaboradores = await resp.json();

    this.colaboradores = listaColaboradores;
    this.colaboradoresTotal = listaColaboradores;
    console.log(this.colaboradores);
  }

  filtrarFTE(evt) {
    if (this.shadowRoot.getElementById("swtFTE").checked) {
      this.colaboradores = this.colaboradores.filter(function(colaborador) {
        return colaborador.fte == 1;
      });
    } else {
      this.colaboradores = this.colaboradoresTotal;
    }
  }

  render() {
    return html`
      <style>
        :host {
          --mdc-theme-primary: #072146;
          --mdc-theme-text-primary-on-light: #072146;
        }
      </style>

      <div class="centered">
        <paper-card
          heading="Colaboradores ${this.isAsignado
            ? ""
            : "no "} asignados - Disciplina: ${this.disciplina}"
          alt="colaborador"
        >
          <div>
            <div class="filtrofte">
              <mwc-switch id="swtFTE" }></mwc-switch> <span>FTE = 1</span>
            </div>

            <div class="colaborador-grid">
              <mwc-list type="two-line" avatarList singleSelection>
                ${this.colaboradores.map(
                  (i, index) =>
                    html`<a href="#!/staffing/colaborador/${i.userID}">
                      <mwc-list-item type="two-line" avatarList>
                        <iron-image
                          slot="graphic"
                          class="foto-perfil-list"
                          sizing="contain"
                          alt="profile"
                          preload
                          src="${imgProfile}"
                        ></iron-image>
                        <span slot="primary"
                          >${i.userID + " - " + i.nombre
                           
                           + (i.apepaterno == undefined ? '' : " " + i.apepaterno)
                           + (i.apematerno == undefined ? '' : " " + i.apematerno)
                        
                        }</span
                        >
                        ${this.isAsignado
                          ? html`
                              <span slot="secondary"
                                >${"Proyecto: " + i.proyecto}</span
                              >
                            `
                          : html`
                              <span slot="secondary"
                                >${"Especialidad: " + i.especialidad}</span
                              >
                            `}
                        ${this.isAsignado
                          ? html`
                              <span slot="meta">FTE: ${i.fte}</span>
                            `
                          : html`
                              <mwc-icon-button
                                icon="play_arrow"
                                slot="meta"
                              ></mwc-icon-button>
                            `}
                      </mwc-list-item></a>

                      <mwc-list-divider padded></mwc-list-divider>
                    `
                )}
              </mwc-list>
            </div>
          </div>
        </paper-card>
      </div>
    `;
  }
}

customElements.define("card-report-colaborador", CardReportColaborador);
