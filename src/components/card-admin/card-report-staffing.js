import { LitElement, html, css } from "lit-element";
import { flexFactor } from "@collaborne/lit-flexbox-literals";
import { until } from "lit-html/directives/until";

import "@polymer/paper-card/paper-card.js";
import "@authentic/mwc-select/mwc-select.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-textfield/mwc-textfield.js";
import "@authentic/mwc-button/mwc-button.js";
import "@authentic/mwc-circular-progress/mwc-circular-progress.js";
import { urlAPI } from "../apiconfig.js";

import "@authentic/mwc-table/mwc-table.js";
import Navigo from "navigo";

class CardReportStaffing extends LitElement {
  static get properties() {
    return {
      reporteRender: { type: String }
    };
  }

  constructor() {
    super();

    this.columns = [
      {
        title: "Disciplina",
        field: "disciplina",
        description: "Nombre Disciplina"
      },
      {
        title: "Asignado",
        field: "asignado",
        align: "right",
        description: "Colaboradores asignados durante el PI."
      },
      {
        title: "No Asignado",
        field: "no_asignados",
        align: "right",
        description: "Colaboradores asignados durante el PI."
      }
    ];

    this.data = [
      {
        disciplina: "CIB + Enterprise Systems",
        asignado: "50",
        no_asignados: "10"
      },
      { disciplina: "Coorporte Systems", asignado: "30", no_asignados: "15" }
    ];

    this.reporteRender = "";
  }

  static get styles() {
    return css`
      paper-card {
        width: 100%;
        border-radius: 15px;
      }

      .flex-container {
        padding: 0;
        margin: 0;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
      }

      .centered-table {
        max-width: 80%;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 25px;
        margin-top: 25px;
      }

      .filtro {
        padding-left: 10px;
        padding-right: 10px;
        width: auto;
        ${flexFactor}
      }

      .search {
        width: 65%;
        margin-left: auto;
        margin-right: auto;
      }

      .search-btn {
        text-align: center;
        --mdc-theme-primary: #1973b8;
      }

      .colaborador-grid {
        margin-top: 30px;
        width: 90%;
        margin-left: auto;
        margin-right: auto;
      }

      iron-image.foto-perfil-list {
        width: 56px;
        height: 56px;
        border-radius: 50%;
      }

      mwc-circular-progress--primary {
        color: #072146;
      }
    `;
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  render() {
    this.reporteRender = fetch(urlAPI + "staff/reporte/1")
      .then(response => response.json())
      .then(function(disciplinas) {
        let dataTable = "";
        disciplinas.map(registroReporte => {
          dataTable +=
            "<tr><td>" +
            registroReporte.disciplina +
            '</td><td align="center">' +
            '<a href="#!/staffing?asignado=true&disciplina=' +
            registroReporte.disciplina +
            '">' +
            registroReporte.cantidadAsignados +
            "</a>" +
            '</td><td align="center"><a href="#!/staffing?asignado=false&disciplina=' +
            registroReporte.disciplina +
            '">' +
            registroReporte.cantidadNoAsignados +
            "</a></td></tr>";
        });

        return html`
          <mwc-table id="table_1">
            <script id="template" type="text/template">
              <thead>
                <tr>
                  <th>Disciplina</th>
                  <th align="center">Asignados</th>
                  <th align="center">No Asignados</th>
                </tr>
              </thead>
              <tbody>
                    ${dataTable}
              </tbody>
            </script>
          </mwc-table>
        `;
      });

    return html`
      <style>
        :host {
          --mdc-theme-primary: #072146;
          --mwc-circular-progress--primary: #072146;
        }

        .inline {
          display: flex;
          justify-content: center;
          margin-bottom: 40px;
          --mdc-theme-primary: #072146;
        }
      </style>

      <div class="centered">
        <paper-card heading="REPORTE STAFFING" alt="report_staffing">
          <div class="centered-table">
            ${until(
              this.reporteRender,
              html`
                <div class="inline">
                  <mwc-circular-progress></mwc-circular-progress>
                </div>
              `
            )}
          </div>
        </paper-card>
      </div>
    `;
  }
}

customElements.define("card-report-staffing", CardReportStaffing);
