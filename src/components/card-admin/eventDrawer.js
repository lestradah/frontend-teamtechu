


export function openDrawer(element){

    const drawer = element.shadowRoot.querySelector("mwc-drawer");
    const container = drawer.parentNode;
    container.addEventListener("nav", e => {
      drawer.open = !drawer.open;
    });

}