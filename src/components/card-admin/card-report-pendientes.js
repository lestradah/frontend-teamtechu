import { LitElement, html, css } from "lit-element";
import { flexFactor } from "@collaborne/lit-flexbox-literals";

import "@polymer/paper-card/paper-card.js";
import "@authentic/mwc-select/mwc-select.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-textfield/mwc-textfield.js";
import "@authentic/mwc-button/mwc-button.js";
import { urlAPI } from "../apiconfig.js";

const imgProfile = new URL("../../images/profile_img.png", import.meta.url).toString();
const apiColaborador = urlAPI + "collaborator/";


class CardReportPendientes extends LitElement {
  static get properties() {
    return {
      colaboradoresPendientes: { type: Array }
    };
  }

  constructor() {
    super();
    this.colaboradoresPendientes = [];
  }

  static get styles() {
    return css`
      paper-card {
        width: 100%;
        border-radius: 15px;
      }

      .flex-container {
        padding: 0;
        margin: 0;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
      }

      .filtro {
        padding-left: 10px;
        padding-right: 10px;
        width: auto;
        ${flexFactor}
      }

      .search {
        width: 65%;
        margin-left: auto;
        margin-right: auto;
      }

      .search-btn {
        text-align: center;
        --mdc-theme-primary: #1973b8;
      }

      .colaborador-grid {
        margin-top: 30px;
        width: 90%;
        margin-left: auto;
        margin-right: auto;
      }

      iron-image.foto-perfil-list {
        width: 56px;
        height: 56px;
        border-radius: 50%;
      }

      a {
        text-decoration: none;
        color: #072146;
      }
    `;
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  firstUpdated() {
    this.poblarDataPendientes();
  }

  async poblarDataPendientes() {
    let resp = await fetch(apiColaborador + "?modificado=" + true);
    let listaColaboradores = await resp.json();
    this.colaboradoresPendientes = listaColaboradores;
  }

  render() {
    return html`
      <div class="centered">
        <paper-card
          heading="COLABORADORES POR VALIDAR SKILLS"
          alt="colaborador"
        >
          <div>
            <style></style>

            <div class="colaborador-grid">
              <mwc-list type="two-line" avatarList singleSelection>
                ${this.colaboradoresPendientes.map(
                  (i, index) =>
                    html`<a href="#!/pendientes/colaborador/${i.userID}">
                      <mwc-list-item type="two-line" avatarList>
                        <iron-image
                          slot="graphic"
                          class="foto-perfil-list"
                          sizing="contain"
                          alt="profile"
                          preload
                          src="${imgProfile}"
                        ></iron-image>
                        <span slot="primary"
                          >${i.userID + " - " + i.nombre + " " + i.apepaterno + " " + i.apematerno}</span
                        >
                        <span slot="secondary"
                          >${"Proyecto: " + i.especialidad}</span
                        >
                        <mwc-icon-button
                          icon="play_arrow"
                          slot="meta"
                        ></mwc-icon-button>
                      </mwc-list-item></a>

                      <mwc-list-divider padded></mwc-list-divider>
                    `
                )}
              </mwc-list>
            </div>
          </div>
        </paper-card>
      </div>
    `;
  }
}

customElements.define("card-report-pendientes", CardReportPendientes);
