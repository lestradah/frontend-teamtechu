import { LitElement, html, css } from "lit-element";
import { flexFactor } from "@collaborne/lit-flexbox-literals";

import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import "@polymer/iron-image/iron-image.js";

import "@polymer/paper-button/paper-button.js";
import "@polymer/iron-icons/iron-icons.js";
import "@polymer/paper-styles/color.js";

// import "@material/mwc-top-app-bar";
import "@authentic/mwc-top-app-bar/mwc-top-app-bar.js";
import "@authentic/mwc-icon-button";
import "@authentic/mwc-icon";
import "@authentic/mwc-fab/mwc-fab.js";
import "@authentic/mwc-ripple/mwc-ripple.js";


import '@polymer/iron-ajax/iron-ajax.js';
import { urlAPI } from "../apiconfig.js";

const logoBBVA = new URL('../../images/logo_bbva.png', import.meta.url).toString();
const imgProfile = new URL('../../images/profile_img.png', import.meta.url).toString();
const dirCinturones = '../../images/cinturones/';

const userAPI = urlAPI + "logout/";
const collaboratorAPI = urlAPI + "collaborator/";
const ninjaAPI = urlAPI + "ninja/";
const staffAPI = urlAPI + "staff/";
const techuAPI = urlAPI + "techu/";


class CardColaborador extends LitElement {
  static get properties() {
    return {
      nombreColaborador: String,
      emailColaborador: String,
      anexoColaborador: String,
      buildingBlockColaborador: String,
      disciplinaColaborador: String,
      especialidadColaborador: String,
      liderColaborador: String,
      userid: String,
      mensaje: String,
      colorCinturonNinja: String,
      fechaCinturonNinja: String,
      puntosNinja: Number,
      cursos: String,
      proyectos: String,
      proyectosTodos: String,
      urlreq: String,
      colaborador: Object,
      isValidacion: { type: Boolean },
      seccionValidar: { type: String }
    };
  }

  constructor() {
    super();
    this.urlreq = userAPI;
    this.isValidacion = false;
    this.seccionValidar = html`
      <div class="app-fab--absolute">
        <mwc-fab
          class="light"
          icon="check_circle"
          label="VALIDAR"
          extended
          @click="${this.validarColaborador}"
        ></mwc-fab>
      </div>
    `;
  }

  static get styles() {
    return css`
      iron-image.foto-perfil {
        width: 250px;
        height: 250px;
        padding-right: 10px;
        border-radius: 50%;
      }

      iron-image.logo-bbva {
        width: 150px;
        height: 50px;
      }

      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
      }

      .datos-contacto {
        ${flexFactor}
        margin-left: 20px;
        font-size: 1.5em;
      }

      .container img {
        max-width: 100%;
        max-height: 100%;
      }

      .datos-contacto h3 {
        font-weight: 800;
        margin: 24px 0;
        font-size: 1em;
      }

      .datos-contacto p {
        line-height: 1.5;
        font-size: 0.7em;
      }

      @media (max-width: 800px) {
        datos-contacto {
          font-size: 1em;
        }

        iron-image.foto-perfil {
          width: 150px;
          height: 150px;
        }

        .datos-contacto h3 {
          font-weight: 800;
          margin: 6px 0;
          font-size: 1em;
        }

        .datos-contacto p {
          font-weight: 200;
          margin: 6px 0;
          font-size: 0.6em;
        }
      }

      @media (max-width: 600px) {
        iron-image.foto-perfil {
          width: 120px;
          height: 120px;
        }

        .container h3 {
          font-weight: 800;
          margin: 8px 0;
          font-size: 0.6em;
        }

        .container p,
        span {
          font-weight: 200;
          margin: 8px 0;
          font-size: 0.5em;
        }
      }

      @media (max-width: 506px) {
        .datos-contacto {
          margin-left: 7px;
        }

        iron-image.foto-perfil {
          width: 115px;
          height: 115px;
        }

        .datos-contacto h3 {
          font-size: 0.6em;
          margin: 5px 0;
        }

        .datos-contacto p {
          font-size: 0.4em;
          font-weight: 200;
          margin: 5px 0;
        }

        .center-middle {
          margin-top: auto;
          margin-bottom: auto;
          margin-left: auto;
          margin-right: auto;
          text-align: center;
        }
      }

      .detalle-programas-bbva {
        width: 260px;
      }

      .detalle-programas-bbva .row {
        padding-left: 5px;
        height: 30px;
        position: relative;
      }

      .vertical-center {
        margin: 0;
        position: absolute;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
      }

      @media screen and (min-width: 1131px) {
        .detalle-programas-bbva {
          border-left: 3px solid #1d73b2;
        }
      }

      @media screen and (max-width: 1130px) {
        .detalle-programas-bbva {
          margin-left: 4px;
          display: flex;
          ${flexFactor}
        }

        .techu {
          margin-left: auto;
          margin-right: auto;
        }
      }

      .titulo {
        text-align: center;
        color: #ffffff;
        font-family: "Roboto Mono", monospace;
        font-size: 1.5rem;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }

      paper-card.card-perfil {
        margin-top: 40px;
      }

      .cinturon {
        width: 50px;
        height: 40px;
      }

      .flex-container {
        padding: 0;
        margin: 0;
        list-style: none;

        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;

        -webkit-flex-flow: row wrap;
      }

      .agile {
        ${flexFactor}
        margin-left: 8px;
        margin-right: 8px;
      }

      .center-icon-text {
        display: flex;
        align-items: center;
      }

      .view {
        margin-bottom: 50px;
        position: relative;
        width: 100%;
        display: flex;
        flex-direction: column;
        overflow: hidden;
      }

      .center-middle {
        margin-top: auto;
        margin-bottom: auto;
      }

      .container {
        width: 90%;
        margin-left: auto;
        margin-right: auto;
      }

      .ninja {
        padding-left: 15px;
      }

      .techu {
        padding-left: 15px;
      }

      .lowerCase {
        text-transform: lowercase;
      }
    `;
  }

  render() {
    return html`
      <style>
        .mwc-top-app-bar {
          left: 0;
        }
        :host {
          --mdc-theme-primary: #072146;
        }

        .light {
          --mdc-theme-on-primary: white;
          --mdc-theme-primary: #028484;
          --mdc-theme-on-secondary: white;
          --mdc-theme-secondary: #028484;
        }

        .app-fab--absolute {
          position: fixed;
          bottom: 1rem;
          right: 1rem;
          z-index: 1;
        }

        @media (min-width: 1024px) {
          .app-fab--absolute {
            bottom: 1.5rem;
            right: 1.5rem;
            z-index: 1;
          }
        }
      </style>

      <div class="view">
        <mwc-top-app-bar>
          <mwc-icon-button icon="menu" slot="navigationIcon"></mwc-icon-button>
          <div slot="title" id="title" class="row">
            <div class="flex-container">
              <iron-image
                class="logo-bbva"
                sizing="contain"
                alt="BBVA logo."
                src="${logoBBVA}"
              ></iron-image>
              <span style="margin-left: 10px"></span>
              <div style="margin-top:auto">SISTEMA DE GESTION DE SKILLS</div>
            </div>
          </div>

          <mwc-icon-button
            icon="exit_to_app"
            slot="actionItems"
            @click="${this.logout}"
          ></mwc-icon-button>
        </mwc-top-app-bar>
      </div>

      <div class="centered">
        <div class="container">
          <div></div>
        </div>

        <paper-card
          alt="${this.nombreColaborador}"
          elevation="3"
          class="card-perfil"
        >
          <div class="card-content">
            <section id="about">
              <div class="flex-container container">
                <div class="center-middle">
                  <iron-image
                    class="foto-perfil"
                    sizing="contain"
                    alt="The Polymer logo."
                    preload
                    src="${imgProfile}"
                  ></iron-image>
                </div>

                <div class="datos-contacto center-middle">
                  <h3>${this.nombreColaborador}</h3>
                  <p>Email: ${this.emailColaborador}</p>
                  <p>Anexo: ${this.anexoColaborador}</p>
                </div>

                <div class="detalle-programas-bbva">
                  <div class="ninja">
                    <h3 class="row">Ninja</h3>
                    <div>
                      <span class="row"
                        >Cinturon:
                        <iron-image
                          class="cinturon"
                          sizing="contain"
                          alt="${this.fechaCinturonNinja}"
                          src="${
                            this.colorCinturonNinja == undefined ? '' : 
                            new URL(dirCinturones + this.colorCinturonNinja, import.meta.url).toString()
                            }"
                        ></iron-image
                      ></span>
                    </div>

                    <p class="row">Desde: ${this.fechaCinturonNinja}</p>
                    <p class="row">Puntos Ninja: ${this.puntosNinja}</p>
                  </div>
                  <div class="techu">
                    <h3 class="row">TechU</h3>
                    ${this.cursos}
                  </div>
                </div>
              </div>
            </section>
            <section id="agile-proyectos">
              <div class="flex-container container">
                <div class="agile">
                  <h4>Building Block</h4>
                  <p>${this.buildingBlockColaborador}</p>
                </div>
                <div class="agile">
                  <h4>Proyectos</h4>
                  ${this.proyectos}
                </div>
              </div>
            </section>
            <section id="responsable">
              <div class="flex-container container">
                <div class="agile">
                  <h4>Disciplina / Especialidad</h4>
                  <p>
                    ${this.disciplinaColaborador +
                      " / " +
                      this.especialidadColaborador}
                  </p>
                </div>
                <div class="agile">
                  <h4>Lider</h4>
                  <p>${this.liderColaborador}</p>
                </div>
              </div>
            </section>
          </div>
          <div class="card-actions">
            <paper-button>GitHub</paper-button>
          </div>
        </paper-card>
      </div>

      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>

      <paper-dialog id="proyectoModal" modal with-backdrop>
        <h2>Proyectos</h2>
        <p>${this.proyectosTodos}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>

      <paper-dialog id="alertValidacionColaborador" with-backdrop>
        <h2>Mensaje</h2>
        <p>Colaborador Validado</p>
        <div class="buttons">
          <paper-button dialog-dismiss autofocus>Aceptar</paper-button>
        </div>
      </paper-dialog>

      ${this.isValidacion ? this.seccionValidar : html``}

      <iron-ajax
        auto
        url="${collaboratorAPI + this.userid}"
        handle-as="json"
        @response="${this.loadCollaborator}"
        content-type="application/json"
        @error="${this.loadFailed}"
        debounce-duration="500"
      >
      </iron-ajax>

      <iron-ajax
        auto
        url="${ninjaAPI + this.userid}"
        handle-as="json"
        @response="${this.loadNinja}"
        content-type="application/json"
        @error="${this.loadFailed}"
        debounce-duration="500"
      >
      </iron-ajax>

      <iron-ajax
        auto
        url="${techuAPI + this.userid}"
        handle-as="json"
        @response="${this.loadTechu}"
        content-type="application/json"
        @error="${this.loadFailed}"
        debounce-duration="500"
      >
      </iron-ajax>

      <iron-ajax
        auto
        url="${staffAPI + this.userid}"
        handle-as="json"
        @response="${this.loadStaff}"
        content-type="application/json"
        @error="${this.loadFailed}"
        debounce-duration="500"
      >
      </iron-ajax>
    `;
  }

  async validarColaborador() {
    let dialogValidacion = this.shadowRoot.getElementById(
      "alertValidacionColaborador"
    );

    await this.getColaborador();

    delete this.colaborador.modificado;

    let resp = await fetch(collaboratorAPI + this.userid, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(this.colaborador)
    });

    if (resp.ok) {
      if (resp.status == 200) {
        let data = await resp.json();
        dialogValidacion.open();
        this.seccionValidar = html``;
      } else {
        this.loadFailed();
      }
    }
  }

  loadCollaborator(e) {
    this.colaborador = e.detail.response;
    this.nombreColaborador =
      this.colaborador.nombre +
      " " +
      this.colaborador.apepaterno +
      " " +
      this.colaborador.apematerno;
    this.emailColaborador = this.colaborador.email;
    this.anexoColaborador = this.colaborador.anexo;
    this.buildingBlockColaborador = this.colaborador.building_block;
    this.disciplinaColaborador = this.colaborador.disciplina;
    this.especialidadColaborador = this.colaborador.especialidad;
    this.liderColaborador = this.colaborador.lider;
  }

  async getColaborador() {
    let resp = await fetch(collaboratorAPI + this.userid);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
      } else {
        this.loadFailed();
      }
    }
  }

  loadFailed(e) {
    if (e.detail.request.xhr.response) {
      this.mensaje = e.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }

  loadNinja(e) {
    var ninja = e.detail.response;
    this.fechaCinturonNinja = ninja.fecha_cinturon;
    this.puntosNinja = ninja.puntos;
    this.colorCinturonNinja = this.getBelt(ninja.cinturon);
  }

  loadTechu(e) {
    var techu = e.detail.response;
    this.cursos = techu.cursos.map(
      item => html`
        <p class="row">
          <paper-button>
            <mwc-icon>star</mwc-icon>
            ${item.nombre}
          </paper-button>
        </p>
      `
    );
  }

  loadStaff(e) {
    var staff = e.detail.response;
    var dataStaff = staff.length > 0 ? staff : [];
    let i = 3;
    if (dataStaff.length > 3) {
      this.proyectos = dataStaff.map(
        item => html`
          ${i > 0
            ? html`
                <p class="center-icon-text ${--i}">
                  <mwc-icon>work</mwc-icon>
                  <span style="margin-left: 8px;">${item.proyecto}</span>
                </p>
              `
            : html``}
        `
      );
      this.proyectos.push(html`
        <paper-button class="lowerCase" @click="${this.showProjects}"
          >Ver más...</paper-button
        >
      `);
    } else if (dataStaff.length < 3 && dataStaff.length > 0) {
      this.proyectos = dataStaff.map(
        item => html`
          ${i <= 3
            ? html`
                <p class="center-icon-text ${--i}">
                  <mwc-icon>work</mwc-icon>
                  <span style="margin-left: 8px;">${item.proyecto}</span>
                </p>
              `
            : html``}
        `
      );
    } else {
      this.proyectos = html`
        <p class="center-icon-text">
          <mwc-icon>work</mwc-icon>
          <span style="margin-left: 8px;">No se encontraron datos</span>
        </p>
      `;
    }

    this.proyectosTodos = dataStaff.map(
      item => html`
        <p>
          <span>${item.proyecto}</span>
        </p>

        <iron-ajax
          id="logoutAjax"
          url="${this.urlreq}"
          @handle-as="json"
          loading="${this.loading}"
          method="POST"
          content-type="application/json"
          @response="${this.logoutSuccess}"
          @error="${this.logoutFailed}"
          debounce-duration="500"
        >
        </iron-ajax>

        <paper-dialog id="errorModal" modal with-backdrop>
          <h2>Mensaje</h2>
          <p>${this.mensaje}</p>
          <div class="buttons">
            <paper-button dialog-confirm autofocus>Cerrar</paper-button>
          </div>
        </paper-dialog>
      `
    );
  }

  showProjects(e) {
    this.shadowRoot.getElementById("proyectoModal").opened = true;
  }

  getBelt(color) {
    var ico;
    switch (color) {
      case "Blanco":
        ico = "belt-white.svg";
        break;
      case "Amarillo":
        ico = "belt-yellow.svg";
        break;
      case "Verde":
        ico = "belt-green.svg";
        break;
      case "Azul":
        ico = "belt-blue.svg";
        break;
      case "Marron":
        ico = "belt-brown.svg";
        break;
      case "Negro":
        ico = "belt-black.svg";
        break;
    }
    return ico;
  }

  logout() {
    var jrequest = {
      userID: this.userid
    };
    this.shadowRoot.getElementById("logoutAjax").body = jrequest;
    this.shadowRoot.getElementById("logoutAjax").generateRequest();
  }

  logoutSuccess(event) {
    location.href = location.href.replace(location.hash, "");
  }

  logoutFailed(event, error) {
    if (event.detail.request.xhr.response) {
      this.mensaje = event.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }
}

customElements.define("card-colaborador", CardColaborador);
