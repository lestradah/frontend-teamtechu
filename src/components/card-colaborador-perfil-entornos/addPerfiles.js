
export function loadEvents(element){

    document.addEventListener("DOMContentLoaded", event => {
        
        let domBind = element.shadowRoot.querySelector("dom-bind#objectInputData");

        //GET ALL APPS
        var apps = [
          {
            nombre: "Developer",
          },
          {
            nombre: "Functional Analyst",
          },
          {
            nombre: "Quality Assurance",
          },
          {
            nombre: "Architec",
          }
        ];

        domBind.source = apps;

        //GET PERFILES COLABORADOR
        domBind.items = [];

        domBind.addSelected = evt => {
          domBind.$.objectChips.add(evt.detail.value);
          //put perfiles 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").clear();
            // evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").blur();
          }, 100);
        };
      });

}