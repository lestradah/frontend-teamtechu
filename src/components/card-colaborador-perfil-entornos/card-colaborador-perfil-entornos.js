import { LitElement, html, css } from "lit-element";

//import "@polymer/iron-input/iron-input.js";
import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";

import "@authentic/mwc-select";
import "@authentic/mwc-list/mwc-list.js";
import "@authentic/mwc-list/mwc-list-item.js";
import "@authentic/mwc-list/mwc-list-divider.js";
import "@authentic/mwc-menu/mwc-menu.js";
import "@authentic/mwc-icon";

import "@fluidnext-polymer/paper-chip/paper-chips.js";
import "@fluidnext-polymer/paper-autocomplete";
import '@polymer/iron-ajax/iron-ajax.js';

import { until } from "lit-html/directives/until";

import { loadEvents } from "./addPerfiles.js";
import { urlAPI } from "../apiconfig.js";

const collaboratorAPI = urlAPI + "collaborator/";

class CardColaboradorPerfilEntornos extends LitElement {
  static get properties() {
    return {
      nombreColaborador: { type: String },
      emailColaborador: { type: String },
      selectedItems: { type: Array },
      userID: String,
      perfilesColaborador: Array,
      entornos: Array,
      niveles: String,
      colaborador: Object
    };
  }

  constructor() {
    super();
    this.entornos = [];
  }

  connectedCallback() {
    super.connectedCallback();
    this.entorno = html`<span>Cargando</span>`;
    loadEvents(this);
  }

  disconnectedCallback() {
    this.entorno = html`<span>Cargando</span>`;
    super.disconnectedCallback();
  }

  static get styles() {
    return css`
      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }
    `;
  }

  firstUpdated() {
    this.loadCollaborator();
  }

  render() {

    console.log('render');
    return html`
      <style>
        paper-card {
          --paper-card-header-text: {
            color: #ffffff;
            background-color: #1973b8;
            font-weight: 600;
          }
        }

        :host {
          --paper-chip-color: #1973b8;
          --mdc-theme-primary: #072146;
          --mdc-select-bottom-line-hover-color: #072146;
        }
      </style>

      <div class="centered">
        <paper-card heading="Perfil - Entornos" alt="Perfil Entornos">
          <div class="card-content">
            <style>
              paper-autocomplete#dataInputObjects {
                --paper-input-font-color: #1d73b2;
                --paper-autocomplete-main-color: #072146;
              }

              .chipsContainer {
                height: auto;
                overflow: auto;
              }
            </style>

            <!-- Conocimiento Funcional -->
            <h4>Perfil</h4>
            <dom-bind id="objectInputData">
              <template>
                  <div class="chipsContainer">
                      <paper-chips id="objectChips" items="{{items}}" text-property="nombre"></paper-chips>
                  </div>
                  <paper-autocomplete id="profileInputObjects"
                    show-results-on-focus="true" 
                    label="Ingresar perfil" 
                    text-property="nombre" 
                    source="{{source}}" 
                    on-autocomplete-selected="addSelected">
                      <template id="customTemplate" slot="autocomplete-custom-template">
                          <link rel="stylesheet" href="../../../style-sugerencias.css">
                          <paper-item class="custom-item" on-tap="_onSelect" id$="[[_getSuggestionId(index)]]" role="option" aria-selected="false" >
                              <div class="container info" index="[[index]]">
                                  <div>[[item.nombre]]</div>
                              </div>
                              <paper-ripple></paper-ripple>
                          </paper-item>
                      </template>
                  </paper-autocomplete>
              </template>
            </dom-bind>
            <div>${this.entornos}</div>
          </div>
        </paper-card>
      </div>

      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>
    `;
  }



  async getColaborador() {
    let resp = await fetch(collaboratorAPI + this.userID);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
      } else {
        this.loadFailed();
      }
    }
  }

  async loadCollaborator() {

    let resp = await fetch(collaboratorAPI + this.userID);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();

        var perfiles = [
          { id: 1, nombre: "Solutions Architect" },
          { id: 2, nombre: "CI/CD - DevOps" },
          { id: 3, nombre: "Cells" },
          { id: 4, nombre: "Cells4" },
          { id: 5, nombre: "Cells5" }
        ];

        this.perfilesColaborador = this.colaborador.perfil;
        let objectInputData = this.shadowRoot.getElementById("objectInputData");
        let objectChips = this.shadowRoot.getElementById("objectChips");
        objectInputData.items = this.perfilesColaborador;
        objectInputData.source = perfiles;

        let entornos = this.colaborador.entorno;

        if (entornos != null && entornos.length) {
          this.setNiveles(entornos);
          this.deleteChips(this);
        }

        let __this = this;

        objectInputData.addSelected = (evt) => {
          objectChips.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('profileInputObjects').clear();
              __this.shadowRoot.getElementById('profileInputObjects').blur();
              let chip = objectChips.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

      } else {
        this.loadFailed();
      }
    }

  }

  setNiveles(entornos) {
    let listNiveles = [
      { nivel: 'Experto' },
      { nivel: 'Alto' },
      { nivel: 'Medio' },
      { nivel: 'Bajo' }
    ];
    let locEntornos = [];
    let locNiveles = [];

    entornos.map(function (e, i) {
      locEntornos.push(html`
                <h4>${e.nombre}</h4>
                <mwc-select placeholder="Elija su nivel de conocimiento" id="${e.nombre}" entorno-position="${i}">
                  <mwc-menu>
                  <mwc-list>
                  ${
        locNiveles = listNiveles.map(
          n =>
            html`
                              ${e.nivel == n.nivel ?
                html`<mwc-list-item value="${n.nivel}" selected >${n.nivel}</mwc-list-item>` :
                html`<mwc-list-item value="${n.nivel}">${n.nivel}</mwc-list-item>`}
                            `
        )
        }
              </mwc-list>
            </mwc-menu>
          </mwc-select>`)
    }
    );

    this.entornos = html`<div class="card-content"> ${locEntornos.map(entor => entor)} </div>`;
    this.eventsCbo();
  }


  sendInput(evt) {
    if (evt.code === 'Enter' && evt.target.value && evt.target.value != '') {
      domBind.$.stringChips.add(evt.target.value);
      evt.target.value = null;
    }
  }

  loadFailed(e) {
    if (e.detail.request.xhr.response) {
      this.mensaje = e.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }

  async updatePerfil() {
    await this.getColaborador();
    this.colaborador.perfil = this.shadowRoot.getElementById("objectInputData").items;
    this.colaborador.modificado = true;

    let resp = await fetch(collaboratorAPI + this.userID,
      { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(this.colaborador) });

    if (resp.ok) {
      if (resp.status == 200) {
        let data = await resp.json();
        console.log(data);
      } else {
        this.loadFailed();
      }
    }
  }

  respuestaRecibida(e) {
  }

  deleteChips(e) {
    let card = e;
    let $this = this;
    setTimeout(function (ex) {
      let allPaperChips = card.shadowRoot.getElementById('objectChips').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
    }, 300);
  }

  eventsCbo() {
    let __this = this;
    setTimeout(
      function () {
        let $this = __this;
        __this.shadowRoot.querySelectorAll("mwc-select").forEach(function (select) {
          select.addEventListener("change", function () {
            let index = this.getAttribute('entorno-position');
            let req = {
              nombre: this.id,
              nivel: this.value
            };
            $this.colaborador.entorno[index] = req;
            $this.updatePerfil();
          });
        });
      }, 200);
  }

}

customElements.define(
  "card-colaborador-perfil-entornos",
  CardColaboradorPerfilEntornos
);
