
export function loadEvents(idColaborador, element){

    document.addEventListener("DOMContentLoaded", event => {
        
        console.log(idColaborador);
        
        let domBind = element.shadowRoot.querySelector("dom-bind#objectInputData");
        let domBind2 = element.shadowRoot.querySelector("dom-bind#objectInputData2");


        //GET ALL APPS
        var apps = [
          {
            id: "1",
            nombre: "T-Refiero",
            descripcion: "Sistema de Referidos Leasing",
          },
          {
            id: "2",
            nombre: "Workflow Comex",
            descripcion: "Sistema de Workflow Comex",
          },
          {
            id: "3",
            nombre: "Sigal",
            descripcion: "Sistema de gestion de activos leasing",
          }
        ];

        domBind.source = apps;
        domBind2.source = apps;




        //GET APPS COLABORADOR
        domBind.items = [];
        domBind2.items = [];

        domBind.addSelected = evt => {
          domBind.$.objectChips.add(evt.detail.value);
          //put app 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").clear();
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects").blur();
          }, 100);
        };

        domBind2.addSelected = evt => {
          domBind2.$.objectChips2.add(evt.detail.value);
          //put app 

          setTimeout(function() {
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects2").clear();
            evt.target.shadowRoot.querySelector("paper-autocomplete#dataInputObjects2").blur();
          }, 100);
        };
      });

}