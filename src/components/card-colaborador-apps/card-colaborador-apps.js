import { LitElement, html, css } from "lit-element";

import "@polymer/iron-input/iron-input.js";
import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import "@fluidnext-polymer/paper-chip/paper-chips.js";
import "@fluidnext-polymer/paper-autocomplete";
import { loadEvents } from "./addApp.js";
import { urlAPI } from "../apiconfig.js";

const collaboratorAPI = urlAPI + "collaborator/";

class CardColaboradorApps extends LitElement {
  static get properties() {
    return {
      nombreColaborador: { type: String },
      emailColaborador: { type: String },
      selectedItems: { type: Array },
      userid: String,
      colaborador: Object
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  firstUpdated(){
    this.loadCollaborator();
  }

  static get styles() {
    return css`
      .centered {
        max-width: 85%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
      }

      paper-card {
        width: 100%;
        border-radius: 15px;
      }
    `;
  }

  render() {
    return html`
      <style>
        paper-card {
          --paper-card-header-text: {
            color: #ffffff;
            background-color: #1973b8;
            font-weight: 600;
          }
        }

        :host {
            --paper-chip-color : #1973B8;
        }


      </style>

      <div class="centered">
        <paper-card heading="Aplicaciones" alt="Aplicaciones">
          <div class="card-content">
            <style>
              paper-autocomplete#dataInputObjects,
              paper-autocomplete#dataInputObjects2 {
                --paper-input-font-color: #1d73b2;
                --paper-autocomplete-main-color: #072146;
              }

              .chipsContainer {
                height: auto;
                overflow: auto;
              }
            </style>


            <!-- Conocimiento Funcional -->
            <h4>Funcional<h4> 

            <dom-bind id="objectInputData">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips"
                    items="{{items}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects"
                  label="Elija las aplicaciones con conocimiento funcional"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                <template id="customTemplate" slot="autocomplete-custom-template">
                <link rel="stylesheet" href="../../../style-sugerencias.css">
                <paper-item class="custom-item" on-tap="_onSelect" id$="[[_getSuggestionId(index)]]" role="option" aria-selected="false">
                    <div class="container info" index="[[index]]">
                        <div>[[item.nombre]]</div>
                    </div>
                    <paper-ripple></paper-ripple>
                </paper-item>
            </template>
                </paper-autocomplete>
              </template>
            </dom-bind>

            <!-- Conocimiento Tecnico -->
            <h4>Tecnico<h4> 

            <dom-bind id="objectInputData2">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips2"
                    items="{{items}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects2"
                  label="Elija las aplicaciones con conocimiento tecnico"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                  show-results-on-focus="true"
                >
                  <template id="customTemplate" slot="autocomplete-custom-template">
                      <link rel="stylesheet" href="../../../style-sugerencias.css">
                      <paper-item class="custom-item" on-tap="_onSelect" id$="[[_getSuggestionId(index)]]" role="option" aria-selected="false">
                          <div class="container info" index="[[index]]">
                              <div>[[item.nombre]]</div>
                          </div>
                          <paper-ripple></paper-ripple>
                      </paper-item>
                  </template>
                </paper-autocomplete>
              </template>
            </dom-bind>

            <h4>Distribuido<h4> 
            <dom-bind id="objectInputData3">
              <template>
                <div class="chipsContainer">
                  <paper-chips
                    id="objectChips3"
                    items="{{items}}"
                    text-property="nombre"
                  ></paper-chips>
                </div>
                <paper-autocomplete
                  id="dataInputObjects3"
                  show-results-on-focus="true"
                  label="Elija las aplicaciones con conocimiento distribuido"
                  text-property="nombre"
                  source="{{source}}"
                  on-autocomplete-selected="addSelected"
                >
                  <template id="customTemplate" slot="autocomplete-custom-template">
                      <link rel="stylesheet" href="../../../style-sugerencias.css">
                      <paper-item class="custom-item" on-tap="_onSelect" id$="[[_getSuggestionId(index)]]" role="option" aria-selected="false">
                          <div class="container info" index="[[index]]">
                              <div>[[item.nombre]]</div>
                          </div>
                          <paper-ripple></paper-ripple>
                      </paper-item>
                  </template>
                </paper-autocomplete>
              </template>
            </dom-bind>


          </div>
        </paper-card>
      </div>

      <paper-dialog id="errorModal" modal with-backdrop>
        <h2>Mensaje</h2>
        <p>${this.mensaje}</p>
        <div class="buttons">
          <paper-button dialog-confirm autofocus>Cerrar</paper-button>
        </div>
      </paper-dialog>

    `;
  }

  async getColaborador() {
    let resp = await fetch(collaboratorAPI + this.userid);

    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
        console.log('en getcolaborador');
        console.log(this.colaborador);
      } else {
        this.loadFailed();
      }
    }
  }

  async loadCollaborator(e) {
    let resp = await fetch(collaboratorAPI + this.userid);
    if (resp.ok) {
      if (resp.status == 200) {
        this.colaborador = await resp.json();
        var perfilesDistribuido = [
          { id: 1, nombre: "T-Refiero" },
          { id: 2, nombre: "Workflow Comex" },
          { id: 3, nombre: "SAB" },
          { id: 4, nombre: "SIGAL" }
        ];

        var perfilesTecnicos = [
          { id: 1, nombre: "Frontend" },
          { id: 2, nombre: "Backend" },
          { id: 3, nombre: "APis & Services" },
          { id: 4, nombre: "Data" }
        ];

        var perfilesFuncionales = [
          { id: 1, nombre: "SGL" },
          { id: 2, nombre: "CARTERA" },
          { id: 3, nombre: "Confirmming" },
          { id: 4, nombre: "Lineas" }
        ];

        var objectInputData = this.shadowRoot.getElementById("objectInputData");
        var objectChips = this.shadowRoot.getElementById("objectChips");
        var objectInputData2 = this.shadowRoot.getElementById("objectInputData2");
        var objectChips2 = this.shadowRoot.getElementById("objectChips2");
        var objectInputData3 = this.shadowRoot.getElementById("objectInputData3");
        var objectChips3 = this.shadowRoot.getElementById("objectChips3");
        let __this = this;

        objectInputData.items = this.colaborador.funcional;
        objectInputData.source = perfilesFuncionales;

        objectInputData.addSelected = (evt) => {
          objectChips.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects').clear();
              __this.shadowRoot.getElementById('dataInputObjects').blur();
              let chip = objectChips.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

        objectInputData2.items = this.colaborador.tecnico;
        objectInputData2.source = perfilesTecnicos;

        objectInputData2.addSelected = (evt) => {
          objectChips2.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects2').clear();
              __this.shadowRoot.getElementById('dataInputObjects2').blur();
              let chip = objectChips2.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips2.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

        objectInputData3.items = this.colaborador.distribuido;
        objectInputData3.source = perfilesDistribuido;

        objectInputData3.addSelected = (evt) => {
          objectChips3.add(evt.detail.value);
          setTimeout(
            function () {
              __this.shadowRoot.getElementById('dataInputObjects3').clear();
              __this.shadowRoot.getElementById('dataInputObjects3').blur();
              let chip = objectChips3.shadowRoot.querySelectorAll('paper-chip');
              let index = chip.length - 1;
              chip[index].onclick = function () {
                objectChips3.remove(index);
                __this.updatePerfil();
              }
            }
            , 100);
          this.updatePerfil();
        };

        this.deleteChips(this);
      } else {
        this.loadFailed();
      }
    }
  }

  loadFailed(e) {
    if (e.detail.request.xhr.response) {
      this.mensaje = e.detail.request.xhr.response.message;
    } else {
      this.mensaje = "Error Interno";
    }
    this.shadowRoot.getElementById("errorModal").opened = true;
  }


  async updatePerfil() {
    await this.getColaborador();
    this.colaborador.funcional = this.shadowRoot.getElementById("objectInputData").items;
    this.colaborador.tecnico = this.shadowRoot.getElementById("objectInputData2").items;
    this.colaborador.distribuido = this.shadowRoot.getElementById("objectInputData3").items;
    this.colaborador.modificado = true;

    let resp = await fetch(collaboratorAPI + this.userid,
      { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(this.colaborador) });

    if (resp.ok) {
      if (resp.status == 200) {
        let data = await resp.json();
        console.log(data);
      } else {
        this.loadFailed();
      }
    }
  }

  deleteChips(e) {
    let card = e;
    let $this = this;
    setTimeout(function (ex) {
      let allPaperChips = card.shadowRoot.getElementById('objectChips').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
      let allPaperChips2 = card.shadowRoot.getElementById('objectChips2').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips2.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips2').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
      let allPaperChips3 = card.shadowRoot.getElementById('objectChips3').shadowRoot.querySelectorAll('paper-chip');
      allPaperChips3.forEach(function (paperChip) {
        paperChip.querySelector('iron-icon').onclick = function () {
          card.shadowRoot.getElementById('objectChips3').remove(paperChip.index);
          $this.updatePerfil();
        }
      });
    }, 300);
  }

  respuestaRecibida(e) {
  }

}



customElements.define("card-colaborador-apps", CardColaboradorApps);
